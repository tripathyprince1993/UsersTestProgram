module.exports = (app) => {
    const Users = require('../controllers/users.controller.js');

    // Create a new Note
    app.post('/Users',  Users.create);

    // Retrieve all Users
    app.get('/Users', Users.findAll);

    // Retrieve a single Note with noteId
    app.get('/Users/:UserId', Users.findOne);

    // Update a Note with noteId
    app.put('/Users/:UserId', Users.update);

    // Delete a Note with noteId
    app.delete('/Users/:UserId', Users.delete);
}