module.exports = (app) => {
    const Category = require('../controllers/category.controller.js');

    // Create a new Note
    app.post('/Category',  Category.create);


    app.get('/Category',  Category.findAll);

}