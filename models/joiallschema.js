const Joi = require('@hapi/joi'); 

Joi.objectId = require('joi-objectid')(Joi)

const UserSchema = Joi.object({
    name: Joi.string()
      .required()
      .min(3)
      .label("name"),
      password: Joi.string()
      .required()
      .min(3)
      .label("password"),
    email: Joi.string()
      .email()
      .required()
      .label("Email"),
    role: Joi.string()
      .required()
      .label("Role"),
      category: Joi.objectId()
      .required()
      .label("category")

  });
  
  const CategorySchema = Joi.object({
      category: Joi.objectId()
      .required()
      .label("category")

  });


module.exports={
    UserSchema,
    CategorySchema
}