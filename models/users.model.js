const mongoose = require('mongoose');

const UsersSchema = mongoose.Schema({
    name: {
        type:String,
        required:true,  
        minlength: 5,
        maxlength: 50,
    },
    email: { 
        type:String,
        required:true,
        minlength: 5,
        maxlength: 50, 
        unique:true
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 999
      },
 
    role:{
        type:String,
        required:true ,
        minlength: 5,
        maxlength: 50,
       
    },
    category:{
        type:mongoose.Schema.Types.ObjectId,
         ref:"Category" ,
         required:true
        },
  
    Status:{
        type:String,
        required:false,
        default:"Active",
        new:true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UsersSchema);