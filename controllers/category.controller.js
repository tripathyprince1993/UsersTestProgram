const Joi = require('joi');
const Category = require('../models/category.model');





// Create and Save a new User
exports.create = (req, res) => {

    const { body } = req;
    console.log(body)
    // Validate request


    if (!req.body.CategoryName) {
        return res.status(400).send({
            message: "Category can not be empty"
        });
    }
    // Create a User
    const Categories = new Category({
        CategoryName: req.body.CategoryName
    });

    // Save User in the database
    Categories.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the User."
            });
        });
};


// Create and Save a new User
exports.findAll = async (req, res, next) => {
    try {
        const CategoryData = await Category.find().select('CategoryName');
        if (CategoryData.length == 0) return res.status(400).send({ message: "No Data Found" });
        res.status(200).send(CategoryData)
    } catch (error) {
        res.status(422).send({ message: error })
        //res.send()
        next(error)
    }
};