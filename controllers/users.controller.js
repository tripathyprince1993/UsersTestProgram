
const User = require('../models/users.model');
const { UserSchema } = require('../models/joiallschema')
const Category = require('../models/category.model');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = 's0/\/\P4$$w0rD';



// Create and Save a new User
exports.create = async (req, res, next) => {
    try {
        const { body } = req;
        // Validate request
        const { values } = await UserSchema.validate(body);
        //console.log(req.body.category.match(/^[0-9a-fA-F]{24}$/))

        const CategoryData = await Category.find({ _id: req.body.category })

        if (CategoryData.length == 0) return res.status(422).send({ message: "Category doesn't exists" });

        const UsersData = await User.find({ email: req.body.email })

        if (UsersData.length > 0) return res.status(422).send({ message: "Email already Registered" });
        
        const hashedPassword = await bcrypt.hash(req.body.password, saltRounds)
      

        const Users = new User({
            name: req.body.name,
            email: req.body.email,
            role: req.body.role,
            category: req.body.category,
            password:   hashedPassword
        });
        // Save User in the database
        Users.save()
            .then(data => {
                res.status(201).send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the User."
                });
            });

    } catch (error) {
        if (error.isJoi === true) {
            error.status = 422;

            res.status(422).send({ message: error.details[0].message })

        }
        //res.send()
        next(error)
    }

};



// Retrieve and return all User from the database.
exports.findAll = async (req, res) => {
    try {
        const UserData = await User.find()
            .select('name email role Status')
            .populate({ path: 'category', select: 'CategoryName' });

            //const match = await bcrypt.compare(password, userData.passwordHash);

        if (UserData.length == 0) return res.status(204).send({ message: "No Data Found" });
        res.status(200).send(UserData)

    } catch (error) {
        res.status(422).send({ message: error })
        //res.send()

    }
};

// Find a single User with a UserId
exports.findOne = (req, res) => {
    User.findById(req.params.UserId)
        .then(User => {
            if (!User) {
                return res.status(404).send({
                    message: "User not found with id " + req.params.UserId
                });
            }
            res.send(User);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with id " + req.params.UserId
                });
            }
            return res.status(500).send({
                message: "Error retrieving User with id " + req.params.UserId
            });
        });
};


// Update a User identified by the UserId in the request
exports.update = async (req, res) => {
    // Validate Request
    try {


        const body = req.body;

        if (Object.keys(body).length === 0) {
            return res.status(400).send({
                message: "User updation data can not be empty"
            });
        }

        if (body.hasOwnProperty('category')) {
            if (!req.body.category.match(/^[0-9a-fA-F]{24}$/)) return res.status(400).send({ message: "Wrong Category id format found" });
            const CategoryData = await Category.find({ _id: req.body.category })

            if (CategoryData.length == 0) return res.status(400).send({ message: "Category doesn't exists" });
        }
        if (body.hasOwnProperty("email")) {
            const UsersData = await User.find({ email: req.body.email })
            if (UsersData.length > 0) return res.status(422).send({ message: "Email Should be unique." });
        }
        if (body.hasOwnProperty("password")){
            const hashedPassword = await bcrypt.hash(req.body.password, saltRounds)
            body.password = hashedPassword
            //console.log("updated password ")
        }

        // Find User and update it with the request body
        User.findByIdAndUpdate(req.params.UserId, body, { new: true })
            .then(User => {
                if (!User) {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.UserId
                    });
                }
                res.send(User);
            }).catch(err => {
                if (err.kind === 'ObjectId') {
                    return res.status(404).send({
                        message: "User not found with id " + req.params.UserId
                    });
                }
                return res.status(500).send({
                    message: "Error updating User with id " + req.params.UserId
                });
            });
    } catch (error) {
        if (error.isJoi === true) {
            error.status = 422;

            res.status(422).send({ message: error.details[0].message })

        }
        //res.send()
        next(error)
    }
};

// Delete a User with the specified UserId in the request
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.UserId)
        .then(User => {
            if (!User) {
                return res.status(404).send({
                    message: "User not found with id " + req.params.UserId
                });
            }
            res.status(200).send({ message: "User deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "User not found with id " + req.params.UserId
                });
            }
            return res.status(500).send({
                message: "Could not delete User with id " + req.params.UserId
            });
        });
};
